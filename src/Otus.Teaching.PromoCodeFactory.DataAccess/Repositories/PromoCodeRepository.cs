﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

public class PromoCodeRepository : EfRepository<PromoCode>, IPromoCodeRepository
{
    public PromoCodeRepository(DatabaseContext context) : base(context)
    {
    }

    public async Task AddRangeAsync(List<PromoCode> promoCodes)
    {
        Context.PromoCodes.AddRange(promoCodes);
        await Context.SaveChangesAsync();
    }
}