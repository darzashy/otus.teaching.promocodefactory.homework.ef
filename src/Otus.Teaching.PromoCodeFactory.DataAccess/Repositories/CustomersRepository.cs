﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using N.EntityFramework.Extensions;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

public class CustomersRepository : EfRepository<Customer>, ICustomerRepository
{
    public CustomersRepository(DatabaseContext context) : base(context)
    {
    }

    public override async Task<Customer> GetByIdAsync(Guid id)
    {
        return await Context.Customers
            .Where(x => x.Id == id)
            .Include(x => x.PromoCodes)
            .Include(x => x.CustomerPreferences)
            .ThenInclude(x => x.Preference)
            .FirstOrDefaultAsync();
    }

    public async Task CreateCustomerAsync(Customer customer, List<Guid> preferencesIds)
    {
        if (await Context.Preferences.CountAsync(x => 
                preferencesIds.Contains(x.Id)) < preferencesIds.Count)
        {
            throw new HttpRequestException("Указано несуществующее предпочтение", null, HttpStatusCode.BadRequest);
        }
        
        Context.Customers.Add(customer);
        Context.CustomerPreferences.AddRange(preferencesIds.Select(x => new CustomerPreference
        {
            Id = Guid.NewGuid(),
            CustomerId = customer.Id,
            PreferenceId = x
        }));

        await Context.SaveChangesAsync();
    }

    public async Task EditCustomerAsync(Customer editedCustomer, List<Guid> preferencesIds)
    {
        await using var transaction = await Context.Database.BeginTransactionAsync();
        
        var customersUpdated = await Context.Customers
            .Where(x => x.Id == editedCustomer.Id)
            .UpdateFromQueryAsync(x => new Customer
            {
                FirstName = editedCustomer.FirstName,
                LastName = editedCustomer.LastName,
                Email = editedCustomer.Email
            });

        if (customersUpdated == 0)
        {
            throw new HttpRequestException("Покупатель не найден", null, HttpStatusCode.NotFound);
        }
        
        if (await Context.Preferences.CountAsync(x => 
                preferencesIds.Contains(x.Id)) < preferencesIds.Count)
        {
            throw new HttpRequestException("Указано несуществующее предпочтение", null, HttpStatusCode.BadRequest);
        }

        await Context.CustomerPreferences
            .Where(x => x.CustomerId == editedCustomer.Id)
            .DeleteFromQueryAsync();

        Context.CustomerPreferences.AddRange(preferencesIds.Select(x => new CustomerPreference
        {
            Id = Guid.NewGuid(),
            CustomerId = editedCustomer.Id,
            PreferenceId = x
        }));

        await Context.SaveChangesAsync();
        await transaction.CommitAsync();
    }

    public async Task DeleteCustomerAsync(Guid id)
    {
        await Context.Customers
            .Where(x => x.Id == id)
            .DeleteFromQueryAsync();

        await Context.PromoCodes
            .Where(x => x.CustomerId == id)
            .DeleteFromQueryAsync();
    }

    public async Task<Guid[]> GetCustomersIdsByPreference(Guid preferenceId)
    {
        return await Context.CustomerPreferences
            .Where(x => x.PreferenceId == preferenceId)
            .Select(x => x.CustomerId)
            .Distinct()
            .ToArrayAsync();
    }
}