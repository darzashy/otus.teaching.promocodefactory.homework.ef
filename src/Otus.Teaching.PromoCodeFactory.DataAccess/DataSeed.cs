﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess;

public static class DataSeed
{
    public static void Seed(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Role>().HasData(FakeDataFactory.Roles);
        modelBuilder.Entity<Employee>().HasData(FakeDataFactory.Employees);
        modelBuilder.Entity<Preference>().HasData(FakeDataFactory.Preferences);
        modelBuilder.Entity<Customer>().HasData(FakeDataFactory.Customers);
        modelBuilder.Entity<CustomerPreference>().HasData(FakeDataFactory.CustomerPreferences);
    }
}