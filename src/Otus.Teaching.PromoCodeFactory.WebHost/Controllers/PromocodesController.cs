﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Produces(typeof(IEnumerable<PromoCodeShortResponse>))]
        public async Task<IActionResult> GetPromocodesAsync(
            [FromServices] IPromoCodeRepository promoCodeRepository)
        {
            if (promoCodeRepository == null) throw new ArgumentNullException(nameof(promoCodeRepository));

            var promoCodes = await promoCodeRepository.GetAllAsync();

            return Ok(promoCodes.Select(x => new PromoCodeShortResponse
            {
                Id = x.Id,
                Code = x.Code,
                ServiceInfo = x.ServiceInfo,
                BeginDate = x.BeginDate.ToString(CultureInfo.CurrentCulture),
                EndDate = x.EndDate.ToString(CultureInfo.CurrentCulture),
                PartnerName = x.PartnerName
            }).ToArray());
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(
            GivePromoCodeRequest request,
            [FromServices] IPromoCodeRepository promoCodeRepository,
            [FromServices] IRepository<Employee> employeeRepository,
            [FromServices] ICustomerRepository customerRepository)
        {
            if (request == null) throw new ArgumentNullException(nameof(request));
            if (promoCodeRepository == null) throw new ArgumentNullException(nameof(promoCodeRepository));

            var employee = (await employeeRepository.GetAllAsync()).First();
            var customersWithPreferenceIds = await customerRepository.GetCustomersIdsByPreference(request.PreferenceId);
            var promoCodes = customersWithPreferenceIds.Select(customerId => new PromoCode
                {
                    ServiceInfo = request.ServiceInfo,
                    PartnerName = request.PartnerName,
                    Code = request.PromoCode,
                    PreferenceId = request.PreferenceId,
                    CustomerId = customerId,
                    EmployeeId = employee.Id
                })
                .ToList();

            await promoCodeRepository.AddRangeAsync(promoCodes);
            return Ok();
        }
    }
}