﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers;

/// <summary>
/// Контроллер предпочтений
/// </summary>
[ApiController]
[Route("api/v1/[controller]")]
public class PreferenceController : ControllerBase
{
    /// <summary>
    /// Получение всех возможных предпочтений
    /// </summary>
    /// <returns>Список возможных предпочтений</returns>
    [HttpGet]
    [Produces(typeof(IEnumerable<PreferenceResponse>))]
    public async Task<IActionResult> GetPreferencesAsync(
        [FromServices] IRepository<Preference> repository)
    {
        var preferences = await repository.GetAllAsync();

        return Ok(preferences.Select(x => new PreferenceResponse
        {
            Id = x.Id,
            Name = x.Name
        }));
    }
}