﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        /// <summary>
        /// Получение покупателей
        /// </summary>
        /// <param name="customerRepository">Сервис покупателей</param>
        /// <returns></returns>
        [HttpGet]
        [Produces(typeof(IEnumerable<CustomerShortResponse>))]
        public async Task<IActionResult> GetCustomersAsync(
            [FromServices] ICustomerRepository customerRepository)
        {
            if (customerRepository == null) throw new ArgumentNullException(nameof(customerRepository));
            var customers = await customerRepository.GetAllAsync();

            return Ok(customers.Select(x => new CustomerShortResponse
            {
                Id = x.Id,
                FirstName = x.FirstName,
                LastName = x.LastName,
                Email = x.Email
            }).ToArray());
        }
        
        /// <summary>
        /// Получение конкретного покупателя
        /// </summary>
        /// <param name="id">Идентификатор покупателя</param>
        /// <param name="customerRepository">Сервис покупателей</param>
        /// <returns>Информация о покупателе</returns>
        /// <exception cref="ArgumentNullException"></exception>
        [HttpGet("{id:guid}")]
        [Produces(typeof(CustomerResponse))]
        public async Task<IActionResult> GetCustomerAsync(
            Guid id,
            [FromServices] ICustomerRepository customerRepository)
        {
            if (customerRepository == null) throw new ArgumentNullException(nameof(customerRepository));
            var customer = await customerRepository.GetByIdAsync(id);

            if (customer == null)
            {
                return NotFound();
            }

            return Ok(new CustomerResponse
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
                PromoCodes = customer.PromoCodes.Select(p => new PromoCodeShortResponse
                {
                    Id = default,
                    Code = null,
                    ServiceInfo = null,
                    BeginDate = null,
                    EndDate = null,
                    PartnerName = null
                }).ToList(),
                PreferenceResponses = customer.CustomerPreferences.Select(x => new PreferenceResponse
                {
                    Id = x.PreferenceId,
                    Name = x.Preference.Name
                }).ToList()
            });
        }
        
        /// <summary>
        /// Создание покупателя
        /// </summary>
        /// <param name="request">Данные для запроса</param>
        /// <param name="customerRepository">Сервис покупателей</param>
        /// <returns>Идентификатор созданного покупателя</returns>
        /// <exception cref="ArgumentNullException"></exception>
        [HttpPost]
        [Produces(typeof(Guid))]
        public async Task<IActionResult> CreateCustomerAsync(
            CreateOrEditCustomerRequest request,
            [FromServices] ICustomerRepository customerRepository)
        {
            if (customerRepository == null) throw new ArgumentNullException(nameof(customerRepository));
            
            var newId = Guid.NewGuid();
            var newCustomer = new Customer
            {
                Id = newId,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email
            };
            await customerRepository.CreateCustomerAsync(newCustomer, request.PreferenceIds);

            return Ok(newId);
        }
        
        /// <summary>
        /// Редактирование покупателя
        /// </summary>
        /// <param name="id">Идентификатор покупателя</param>
        /// <param name="request">Данные для запроса</param>
        /// <param name="customerRepository">Сервис покупателей</param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> EditCustomersAsync(
            Guid id, 
            CreateOrEditCustomerRequest request,
            [FromServices] ICustomerRepository customerRepository)
        {
            if (customerRepository == null) throw new ArgumentNullException(nameof(customerRepository));

            var editedCustomer = new Customer
            {
                Id = id,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email
            };
            await customerRepository.EditCustomerAsync(editedCustomer, request.PreferenceIds);

            return Ok();
        }
        
        /// <summary>
        /// Удаление пользователя
        /// </summary>
        /// <param name="id">Идентификатор пользователя</param>
        /// <param name="customerRepository"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteCustomerAsync(
            Guid id,
            [FromServices] ICustomerRepository customerRepository)
        {
            if (customerRepository == null) throw new ArgumentNullException(nameof(customerRepository));

            await customerRepository.DeleteCustomerAsync(id);

            return Ok();
        }
    }
}