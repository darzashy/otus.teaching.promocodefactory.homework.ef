﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Employee
        : BaseEntity
    {
        [Column("role_id")]
        public Guid RoleId { get; set; }
        
        [Column("first_name")]
        [MaxLength(30)]
        public string FirstName { get; set; }
        
        [Column("last_name")]
        [MaxLength(40)]
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        [Column("email")]
        [MaxLength(50)]
        public string Email { get; set; }

        [Column("applied_promocodes_count")]
        public int AppliedPromocodesCount { get; set; }

        #region Relationships

        [ForeignKey(nameof(RoleId))]
        public Role Role { get; set; }

        #endregion
    }
}