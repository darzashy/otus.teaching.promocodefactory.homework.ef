﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Role
        : BaseEntity
    {
        [Column("name")]
        [MaxLength(30)]
        public string Name { get; set; }

        [Column("description")]
        [MaxLength(250)]
        public string Description { get; set; }

        #region Relationships

        public ICollection<Employee> Employees { get; set; }

        #endregion
    }
}