﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

[Table("customer_preferences")]
public class CustomerPreference : BaseEntity
{
    [Column("customer_id")]
    public Guid CustomerId { get; set; }
    
    [Column("preference_id")]
    public Guid PreferenceId { get; set; }

    #region Relationships

    [ForeignKey(nameof(CustomerId))]
    public Customer Customer { get; set; }
    
    [ForeignKey(nameof(PreferenceId))]
    public Preference Preference { get; set; }

    #endregion
}