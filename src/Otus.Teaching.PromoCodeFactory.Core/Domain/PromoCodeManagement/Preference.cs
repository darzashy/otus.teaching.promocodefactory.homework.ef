﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Preference
        :BaseEntity
    {
        [Column("name")]
        [Required]
        [MaxLength(25)]
        public string Name { get; set; }

        #region Relationships
        
        public ICollection<CustomerPreference> CustomerPreferences { get; set; }

        #endregion
    }
}