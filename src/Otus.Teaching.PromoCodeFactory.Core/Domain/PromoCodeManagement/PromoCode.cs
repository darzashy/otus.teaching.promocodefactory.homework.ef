﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class PromoCode
        : BaseEntity
    {
        [Column("customer_id")]
        public Guid CustomerId { get; set; }

        [Column("employee_id")]
        public Guid EmployeeId { get; set; }
        
        [Column("preference_id")]
        public Guid PreferenceId { get; set; }
        
        [Required]
        [Column("code")]
        [MaxLength(100)]
        public string Code { get; set; }

        [Column("service_info")]
        [MaxLength(250)]
        public string ServiceInfo { get; set; }

        [Column("begin_date")]
        public DateTime BeginDate { get; set; }

        [Column("end_date")]
        public DateTime EndDate { get; set; }

        [Column("partner_name")]
        [MaxLength(150)]
        public string PartnerName { get; set; }

        #region Relationships

        [ForeignKey(nameof(EmployeeId))]
        public Employee PartnerManager { get; set; }

        [ForeignKey(nameof(PreferenceId))]
        public Preference Preference { get; set; }
        
        [ForeignKey(nameof(CustomerId))]
        public Customer Customer { get; set; }

        #endregion
    }
}