﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    [Table("customers")]
    public class Customer
        :BaseEntity
    {
        [Column("first_name")]
        [Required]
        [MaxLength(30)]
        public string FirstName { get; set; }
        
        [Column("last_name")]
        [Required]
        [MaxLength(40)]
        public string LastName { get; set; }
        
        [Column("email")]
        [Required]
        [MaxLength(50)]
        public string Email { get; set; }
        
        [Column("phone")]
        [MaxLength(20)]
        public string Phone { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        #region Relationships

        public ICollection<CustomerPreference> CustomerPreferences { get; set; }
        public ICollection<PromoCode> PromoCodes { get; set; }

        #endregion
    }
}