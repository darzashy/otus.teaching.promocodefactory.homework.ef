﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;

public interface ICustomerRepository : IRepository<Customer>
{
    public Task CreateCustomerAsync(Customer customer, List<Guid> preferencesIds);
    Task EditCustomerAsync(Customer editedCustomer, List<Guid> preferencesIds);
    Task DeleteCustomerAsync(Guid id);

    Task<Guid[]> GetCustomersIdsByPreference(Guid preferenceId);
}