﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;

public interface IPromoCodeRepository : IRepository<PromoCode>
{
    Task AddRangeAsync(List<PromoCode> promoCodes);
}